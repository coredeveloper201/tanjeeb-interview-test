<?php

namespace App\Http\Controllers;


use App\Role;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        $this->middleware('auth', ['except' => ['details']]);
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function details()
    {
        return response()->json(User::get(), 200);
    }

    public function admin()
    {
        $user = User::first();
        if ($user->hasRole('admin'))
            return view('admin');
        return redirect('/');
    }

    public function customer()
    {
        $user = User::first();
        if ($user->hasRole('customer'))
            return view('customer');
        return redirect('/');
    }

    public function manager()
    {
        $user = User::first();
        if ($user->hasRole('manager'))
            return view('manager');
        return redirect('/');
    }
}
